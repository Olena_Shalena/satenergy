//
//  SATCircleCollectionViewCell.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 25.03.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATCircleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var graphView: SATCircleGraghView!
    
    var day:SATDay?
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
    }

   
}
