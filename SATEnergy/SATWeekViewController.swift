//
//  SATWeekViewController.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 25.03.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATWeekViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var zoneName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
  
    @IBOutlet weak var myTemperatures: UIButton!
    let reuseIdentifier = "SATCircleCollectionViewCell"
    var areDefaultSetting: Bool = true
    var zones = [SATZone]()
    var currentZone:SATZone?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        zones = SATDataKeeper.sharedInstance.arrayZones
        currentZone = zones[0]
        setup()
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        view.addGestureRecognizer(swipeLeft)
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(7.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                                      for: indexPath as IndexPath) as! SATCircleCollectionViewCell
        let day = (currentZone?.arrayDays?[indexPath.row])!
        cell.day = day
        cell.title.text = SATDay.prefixForDay(index: indexPath.row)
        cell.graphView.currentZoneDays = (currentZone?.arrayDays)!
        cell.graphView.currentDay = day
        cell.graphView.setNeedsDisplay()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let inset:CGFloat = 6.0
        let cellWidth = (self.view.frame.size.width - 4.0 * inset) / 3.0
        let cellHeight = cellWidth
        return CGSize (width:cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "WeeklyTimeShedule", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SATDayViewController") as! SATDayViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setup(){
        navigationItem.title = "Недельное расписание"
        zoneName.text = self.currentZone?.name
        myTemperatures.setTitle("Мои температуры", for: .normal)
        myTemperatures.layer.cornerRadius = 5
    }
    
    func swiped(_ gesture: UISwipeGestureRecognizer) {
        var indexOfZone = self.zones.index(where: {$0 === self.currentZone})
        if (gesture.direction == .left) {
            if (indexOfZone != self.zones.count - 1) {
                indexOfZone = indexOfZone! + 1
            } else {
                indexOfZone = 0
            }
        } else if (gesture.direction == .right) {
            if (indexOfZone != 0) {
                indexOfZone = indexOfZone! - 1
            } else {
                indexOfZone = self.zones.count - 1
            }
        }
        currentZone = self.zones[indexOfZone!]
        zoneName.text = self.currentZone?.name
        collectionView.reloadData()
    }
    
 // MARK: - Actions
    
    @IBAction func myTemperaturesPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "WeeklyTimeShedule", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SATTemperaturesViewController") as! SATTemperaturesViewController
        vc.navigationItem.title = "Мои температуры"
        vc.currentZone = self.currentZone
        navigationController?.pushViewController(vc, animated: true)       
    }
}

