//
//  SATDay.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 25.03.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATDay: NSObject {
    enum weekday: Int {
        case monday
        case tuesday
        case wednesday
        case thursday
        case friday
        case saturday
        case sunday
    }
    var arrayTranspoints = [SATTransPoint]()
    var weekday:weekday?
  
    public class func defaultArrayOfTranspoints(temperatures:[SATTemperature]) -> [SATTransPoint] {
        var arrayTranspoints = [SATTransPoint]()
        
        let beginTime1 = 360 // 6:00
        let transpoint1 = SATTransPoint.init(time: beginTime1, temperatureNumber: 0)
        
        let beginTime2 = 480 //8:00
        let transpoint2 = SATTransPoint.init(time: beginTime2, temperatureNumber: 1)
        
        let beginTime3 = 1200 // 17:00
        let transpoint3 = SATTransPoint.init(time: beginTime3, temperatureNumber: 2)
        
        let beginTime4 = 1320 // 22:00
        let transpoint4 = SATTransPoint.init(time: beginTime4, temperatureNumber: 3)

        arrayTranspoints = [transpoint1!, transpoint2!,transpoint3!, transpoint4!]
        
        return arrayTranspoints
    }

    public class func customArrayOfTranspoints(temperatures:[SATTemperature]) -> [SATTransPoint] {
        var arrayTranspoints = [SATTransPoint]()
        
        let beginTime1 = 420 // 7:00
        let transpoint1 = SATTransPoint.init(time: beginTime1, temperatureNumber: 0)
        
        let beginTime2 = 600 // 10:00
        let transpoint2 = SATTransPoint.init(time: beginTime2, temperatureNumber: 1)
        
        let beginTime3 = 840 // 14:00
        let transpoint3 = SATTransPoint.init(time: beginTime3, temperatureNumber: 2)
        
        let beginTime4 = 1200 // 20:00
        let transpoint4 = SATTransPoint.init(time: beginTime4, temperatureNumber: 3)
        
        let beginTime5 = 1320 // 22:00
        let transpoint5 = SATTransPoint.init(time: beginTime5, temperatureNumber: 4)
        
        arrayTranspoints = [transpoint1!, transpoint2!,transpoint3!, transpoint4!, transpoint5!]
        
        return arrayTranspoints
    }
    
    public class func prefixForDay (index: Int) -> String {
       
        var weekday: String
        switch index {
        case 0:
            weekday = "ПН"
        case 1:
            weekday = "ВТ"
        case 2:
            weekday = "СР"
        case 3:
            weekday = "ЧТ"
        case 4:
            weekday = "ПТ"
        case 5:
            weekday = "СБ"
        case 6:
            weekday = "ВС"
        default:
            weekday = " "
        }
        return weekday
    }

    public class func indexOfCurrentWeekday() -> Int? {
        let todayDate = NSDate()
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate as Date)
        let index = weekDay - 2
        if index == -1 {
            return 6
        }
        return index
    }

    public class func indexOfPreviousWeekday() -> Int? {
        let index = indexOfCurrentWeekday()! - 1
        if index != -1 {
            return index
        } else {
            return 6
        }
    }
}
