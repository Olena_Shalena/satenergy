//
//  SATCircleGraghView.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 25.03.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit


let π:CGFloat = CGFloat(M_PI)
@IBDesignable class  SATCircleGraghView: UIView {
    var currentDay:SATDay?
    var currentZoneDays: [SATDay] = []
    
    override func draw(_ rect: CGRect) {
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2)
        let radius: CGFloat = min (bounds.width, bounds.height) - 10.00
        let arcWidth: CGFloat = 5
        var startAngle: CGFloat
        var endAngle: CGFloat
        var color: UIColor
        let transpoints = currentDay?.arrayTranspoints
        if (transpoints?[0].beginTime)! > 0 {
            let index = previousDayIndex()
            let previousDayTranspoint = currentZoneDays[index].arrayTranspoints.last
            let temperature = SATTemperature.temperatureFrom(transpoint: previousDayTranspoint!)
            color = temperature.color
            startAngle = angle(from: 0)
            endAngle = angle(from: (currentDay?.arrayTranspoints[0].beginTime)!)
            let path = UIBezierPath(arcCenter: center,
                                    radius: radius/2 - arcWidth/2,
                                    startAngle: startAngle,
                                    endAngle: endAngle,
                                    clockwise: true)
            path.lineWidth = arcWidth
            color.setStroke()
            path.stroke()
        }
        for transpoint in transpoints! {
            let temperature = SATTemperature.temperatureFrom(transpoint: transpoint)
            color = temperature.color
            startAngle = angle(from: transpoint.beginTime as Int)
            let startIndex = transpoints?.index(where: {$0 === transpoint})
            let endIndex: Int
            if (startIndex! != (transpoints?.count)! - 1) {
                endIndex = startIndex! + 1
                endAngle = angle(from: (transpoints?[endIndex].beginTime)!)
            } else {
                endAngle = angle(from: 0)
            }
            let path = UIBezierPath(arcCenter: center,
                                    radius: radius/2 - arcWidth/2,
                                    startAngle: startAngle,
                                    endAngle: endAngle,
                                    clockwise: true)
            path.lineWidth = arcWidth
            color.setStroke()
            path.stroke()
        
        }
        color = UIColor.black
        let currentTimeAngle = angle(from: currentTime())
        let currentTimeAngle1 = angle(from: currentTime() + 5)
        
        let currentTimePath = UIBezierPath(arcCenter: center,
                                           radius: radius/2 - arcWidth/2,
                                           startAngle: currentTimeAngle,
                                           endAngle: currentTimeAngle1,
                                           clockwise: true)
        currentTimePath.lineWidth = 15
        color.setStroke()
        currentTimePath.stroke()
    }

    func angle(from time:Int) -> CGFloat {
        let time = SATTime.init(minutes: time)
        let hoursAngle = CGFloat((time?.hours)!) * 2 * π / 24
        let minutesAngle = CGFloat((time?.minutes)!) * 2 * π / 1440
        let angle = hoursAngle + minutesAngle - π / 2
        return angle
    }
    
    func currentTime() -> Int {
    let date = Date()
    let calendar = Calendar.current
    let hours = calendar.component(.hour, from: date)
    let minutes = calendar.component(.minute, from: date)
        return hours * 60 + minutes
    }
    
    func previousDayIndex() -> Int {
        return 5
    }
    
}


