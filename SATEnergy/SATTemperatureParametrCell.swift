//
//  SATTemperatureParametrCell.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 08.04.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATTemperatureParametrCell: UITableViewCell {
    
    enum temperatureParameter:Int {
        case name
        case color
        case value
        case start
    }
    
    @IBOutlet weak var textField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureForRow (at indexPath:IndexPath) {
        let row:temperatureParameter
        row = SATTemperatureParametrCell.temperatureParameter.init(rawValue: indexPath.row)!
        switch row {
            case .name:
            textField.placeholder = "Выберите название"
            case .color:
            textField.placeholder = "Выберите цвет"
            case .value:
            textField.placeholder = "Выберите значение температуры"
            case .start:
            textField.placeholder = "Выберете время начальное время"
            default:
            return
        }
    }
}
