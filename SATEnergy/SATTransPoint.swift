//
//  SATTransPoint.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 13.05.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATTransPoint: NSObject {
    var beginTime: Int
    var temperatureNumber: Int
    
    init?(time: Int, temperatureNumber: Int) {
        self.beginTime = time
        self.temperatureNumber = temperatureNumber
    }

    class func angle(from time:Int) -> CGFloat {
        let time = SATTime.init(minutes: time)
        let hoursAngle = CGFloat((time?.hours)!) * 2 * π / 24
        let minutesAngle = CGFloat((time?.minutes)!) * 2 * π / 1440
        let angle = hoursAngle + minutesAngle - π / 2
        return angle
    }

    class func time(from angle: CGFloat) -> Int {
        let ang = angle + π / 2
        let step = 2 * π / 48
        let numberOfSteps = (ang / step).rounded()
        let time = Int(numberOfSteps * 30)
        return time
    }
 }
