//
//  SATTemperatureTableViewCell.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 09.05.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATTemperatureTableViewCell: UITableViewCell {

    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var color: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    
    var didDeleteButtontappedCallback: ((UITableViewCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        deleteButton.setImage(UIImage.init(named: "delete"), for: .normal)
        name.minimumScaleFactor = 0.5
        name.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureWith(temperature:SATTemperature) {
        value.text = String(temperature.value)
        name.text = temperature.name
        color.backgroundColor = temperature.color
        self.selectionStyle = .none
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        didDeleteButtontappedCallback!(self)
    }
}
