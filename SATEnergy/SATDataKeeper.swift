//
//  SATDataKeeper.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 15.04.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATDataKeeper: NSObject {
        class var sharedInstance :SATDataKeeper {
            struct Singleton {
                static let instance = SATDataKeeper()
            }
            return Singleton.instance
        }
    
    var arrayZones = [SATZone]()
    var currentZone: SATZone?
}
