//
//  SATTemperaturesViewController.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 09.05.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATTemperaturesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var zoneLabel: UILabel!
    
    let reuseIdentifier = "SATTemperatureTableViewCell"
    
    fileprivate var zones: [SATZone] = []
    var currentZone: SATZone?

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        zoneLabel.text = currentZone?.name
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    }
}

extension SATTemperaturesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return (currentZone?.arrayDays?[0].arrayTemperatures.count)!
        return 5
    }


    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! SATTemperatureTableViewCell
//        cell.configureWith(temperature: (currentZone?.arrayDays?[0].arrayTemperatures[indexPath.row])!)
//        cell.didDeleteButtontappedCallback = { [weak self] cell in
//            let indexPath = tableView.indexPath(for: cell)
//            for zone in SATDataKeeper.sharedInstance.arrayZones {
//                if zone == self?.currentZone {
//                    let zoneIndex = SATDataKeeper.sharedInstance.arrayZones.index(of:zone)
//                
//                }
//            }
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {      
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension SATTemperaturesViewController:UIGestureRecognizerDelegate {
    func swiped(_ gesture: UISwipeGestureRecognizer) {
        let zones = SATDataKeeper.sharedInstance.arrayZones
        var indexOfZone = zones.index(where: {$0 === currentZone})
        if (gesture.direction == .left) {
            if (indexOfZone != zones.count - 1) {
                indexOfZone = indexOfZone! + 1
            } else {
                indexOfZone = 0
            }
        } else if (gesture.direction == .right) {
            if (indexOfZone != 0) {
                indexOfZone = indexOfZone! - 1
            } else {
                indexOfZone = zones.count - 1
            }
        }
        currentZone = zones[indexOfZone!]
        zoneLabel.text = currentZone?.name
        tableView.reloadData()
    }
}

