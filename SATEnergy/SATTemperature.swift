//
//  SATTemperature.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 25.03.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATTemperature: NSObject {
    var name:String
    var color:UIColor
    var value:Int
    
    init? (name: String, color: UIColor, value:Int) {
        self.name = name
        self.color = color
        self.value = value
    }

    public class func temperatureFrom(transpoint: SATTransPoint) -> SATTemperature {
        let index = transpoint.temperatureNumber
        return (SATDataKeeper.sharedInstance.currentZone?.arrayTemperatures![index])!
    }
}
