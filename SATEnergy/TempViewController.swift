//
//  TempViewController.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 16.07.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import Foundation
import UIKit

class TempViewController: UIViewController {

    @IBOutlet weak var circlePathsView: SATCirclePathsView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        circlePathsView.initializeButtons()
    }
}
