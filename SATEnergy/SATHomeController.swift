//
//  SATHomeController.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 25.03.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATHomeController: UIViewController {

    @IBOutlet weak var graphView: SATCircleGraghView!
    @IBOutlet weak var zoneName: UILabel!
    @IBOutlet weak var insideTemperature: UILabel!
    @IBOutlet weak var outsideTemperature: UILabel!
    @IBOutlet weak var setTemperature: UIButton!
    @IBOutlet weak var manualButton: UIButton!
    @IBOutlet weak var autoBotton: UIButton!
    @IBOutlet weak var ecoButton: UIButton!
    @IBOutlet weak var forcedButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    
    var temperatures: [SATTemperature] = []
    var day: SATDay?
    var zones: [SATZone] = []
    var currentZone: SATZone?
    var dayIndex:Int? {
        get {
            return SATDay.indexOfCurrentWeekday()
        }
    }

    override func viewDidLoad() {
        self.navigationItem.title = "Sat energy"
        zones = SATDataKeeper.sharedInstance.arrayZones
        currentZone = SATDataKeeper.sharedInstance.arrayZones[0]
        day = SATDataKeeper.sharedInstance.arrayZones[0].arrayDays?[dayIndex!]
        graphView.currentDay = day
        graphView.currentZoneDays = (currentZone?.arrayDays)!
        setupLabels()
        setupButtons()
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        getCurrentTemperature()
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func manualButtonTapped(_ sender: UIButton) {
        view.backgroundColor = UIColor.green
    }
 
    @IBAction func autoButtonTapped(_ sender: UIButton) {
        view.backgroundColor = UIColor.white
    }

    @IBAction func ecoButtonTapped(_ sender: UIButton) {
        view.backgroundColor = UIColor.blue
    }
  
    @IBAction func forcedButtonTapped(_ sender: UIButton) {
        view.backgroundColor = UIColor.red
    }
    
    func setupLabels() {
        zoneName.text = currentZone?.name
        insideTemperature.text = "+20 C"
        outsideTemperature.text = "-10 C"
        setTemperature.setTitle("Button Title",for: .normal)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let dateString = formatter.string(from: date)
        dateLabel.text = NSString(format:"%@ %@", SATDay.prefixForDay(index: dayIndex!), dateString) as String
    }
    
    func setupButtons() {
        manualButton.layer.cornerRadius = 5
        manualButton.layer.borderWidth = 3
        manualButton.layer.borderColor = UIColor.green.cgColor
        manualButton.setTitle("Manual", for: .normal)
        
        autoBotton.layer.cornerRadius = 5
        autoBotton.layer.borderWidth = 3
        autoBotton.layer.borderColor = UIColor.gray.cgColor
        autoBotton.setTitle("Auto", for: .normal)
        
        ecoButton.layer.cornerRadius = 5
        ecoButton.layer.borderWidth = 3
        ecoButton.layer.borderColor = UIColor.blue.cgColor
        ecoButton.setTitle("Eco", for: .normal)
        
        forcedButton.layer.cornerRadius = 5
        forcedButton.layer.borderWidth = 3
        forcedButton.layer.borderColor = UIColor.red.cgColor
        forcedButton.setTitle("Forced", for: .normal)
        
        let temperatureString = String (getCurrentTemperature())
        setTemperature.setTitle(temperatureString, for: .normal)
    }

    func getCurrentTime() -> Int {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        return hour * 60 + minutes
    }
    
    func getCurrentTemperature() -> Int {
        var currentTemperature = 0
        let currentTime = getCurrentTime()
        let lastIndex = (day?.arrayTranspoints.count)! - 1
        if currentTime > (day?.arrayTranspoints[lastIndex].beginTime)! {
            let index = day?.arrayTranspoints[lastIndex].temperatureNumber
            let temperature = SATDataKeeper.sharedInstance.currentZone?.arrayTemperatures?[index!]
            currentTemperature = (temperature?.value)!
        } else {
        
         for i in 0...(day?.arrayTranspoints.count)! - 1 {
            if (day?.arrayTranspoints[i].beginTime)! < currentTime && (day?.arrayTranspoints[i + 1].beginTime)! > currentTime {
                currentTemperature = (SATDataKeeper.sharedInstance.currentZone?.arrayTemperatures?[i].value)!
            }
         }
        }
         return currentTemperature
    }
}

extension SATHomeController:UIGestureRecognizerDelegate {
    func swiped(_ gesture: UISwipeGestureRecognizer) {
        
        var indexOfZone = zones.index(where: {$0 === currentZone})
        if (gesture.direction == .left) {
            if (indexOfZone != zones.count - 1) {
                indexOfZone = indexOfZone! + 1
            } else {
                indexOfZone = 0
            }
        } else if (gesture.direction == .right) {
            if (indexOfZone != 0) {
                indexOfZone = indexOfZone! - 1
            } else {
                indexOfZone = zones.count - 1
            }
        }
        currentZone = zones[indexOfZone!]
        zoneName.text = currentZone?.name
        day = SATDataKeeper.sharedInstance.arrayZones[indexOfZone!].arrayDays?[dayIndex!]
        graphView.currentDay = day
        graphView.currentZoneDays = (currentZone?.arrayDays)!
        let temperatureString = String (getCurrentTemperature())
        setTemperature.setTitle(temperatureString, for: .normal)
        graphView.setNeedsDisplay()
    }
}
