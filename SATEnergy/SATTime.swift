//
//  SATTime.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 05.06.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATTime: NSObject {
    let hours:Int
    let minutes:Int
    init?(minutes:Int) {
        self.hours = Int(minutes / 60)
        self.minutes = minutes % 60
    }
}
