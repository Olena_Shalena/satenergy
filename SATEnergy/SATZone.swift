//
//  SATZone.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 25.03.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

class SATZone: NSObject {
    var name:String?
    var arrayDays:[SATDay]?
    var arrayTemperatures:[SATTemperature]?
    var areDefaultSettings:Bool
    
    init?(areDefaultSettings:Bool) {
       self.areDefaultSettings = areDefaultSettings
    }
    
    public func createArrayDays() -> [SATDay] {
    var arrayDays = [SATDay]()
        for i in 0...6 {
            let day = SATDay()
                if (self.areDefaultSettings) {
                    day.arrayTranspoints = SATDay.defaultArrayOfTranspoints(temperatures: self.arrayTemperatures!)
                } else {
                    day.arrayTranspoints = SATDay.customArrayOfTranspoints(temperatures: self.arrayTemperatures!)
                }
                switch i {
                case 0:
                    day.weekday = .monday
                case 1:
                    day.weekday = .tuesday
                case 2:
                    day.weekday = .wednesday
                case 3:
                    day.weekday = .thursday
                case 4:
                    day.weekday = .friday
                case 5:
                    day.weekday = .saturday
                case 6:
                    day.weekday = .sunday
                default:
                    return arrayDays
                }
            arrayDays.append(day)
        }
        return arrayDays
    }
    
    public func createArrayTemperatures() -> [SATTemperature] {
    var arrayTemperatures = [SATTemperature] ()
        if (self.areDefaultSettings) {
            let value1 = 20
            let temperature1 = SATTemperature.init(name: "Утро", color: .green, value: value1)
            
            let value2 = 15
            let temperature2 = SATTemperature.init(name: "День", color: .blue, value: value2)
            
            let value3 = 24
            let temperature3 = SATTemperature.init(name: "Вечер", color: .orange, value:value3)
            
            let value4 = 22
            let temperature4 = SATTemperature.init(name: "Ночь", color: .red, value:value4)
            
            arrayTemperatures = [temperature1!, temperature2!, temperature3!, temperature4!]
        } else {
            let value1 = 21
            let temperature1 = SATTemperature.init(name: "Я придумал очень-очень-очень-очень длинное название", color: .green, value: value1)
            
            let value2 = 17
            let temperature2 = SATTemperature.init(name: "Никого нет дома", color: .blue, value: value2)
        
            let value3 = 25
            let temperature3 = SATTemperature.init(name: "Дети пришли со школы", color: .orange, value:value3)
           
            let value4 = 23
            let temperature4 = SATTemperature.init(name: "Вечер", color: .red, value:value4)
     
            let value5 = 18
            let temperature5 = SATTemperature.init(name: "Сон", color: .brown, value:value5)
            
            arrayTemperatures = [temperature1!, temperature2!, temperature3!, temperature4!, temperature5!]
        }
        return arrayTemperatures
    }
}
