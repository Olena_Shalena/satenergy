//
//  SATOrbitalPathsView.swift
//  SATEnergy
//
//  Created by Olena Ostrozhynska on 30.05.17.
//  Copyright © 2017 Olena Ostrozhynska. All rights reserved.
//

import UIKit

struct PolarLocation {
    var radius: CGFloat = 0
    var angle: CGFloat = 0
    init (radius: CGFloat, angle: CGFloat) {
        self.radius = radius
        self.angle = angle
    }
}

class SATCirclePathsView: UIView {

    let tangensArray: [CGFloat] = [-7.60, -3.73, -2.41, -1.73, -1.30, -1.00, -0.77, -0.58, -0.41, -0.27, -0.13, 0.00, 0.13, 0.27, 0.41, 0.58, 0.77, 1.00, 1.30, 1.73, 2.41, 3.73, 7.60]
    let anglesArray: [CGFloat] = [-1.44, -1.31, -1.18, -1.05, -0.92, -0.79, -0.65, -0.52, -0.39, -0.26, -0.13, 0.00, 0.13, 0.26, 0.39, 0.52, 0.65, 0.79, 0.92, 1.05, 1.18, 1.31, 1.44]

    var currentDayIndex:Int? {
        return SATDay.indexOfCurrentWeekday()
    }

    var currentZone = SATDataKeeper.sharedInstance.arrayZones[0]
    var currentDay: SATDay? {
        return currentZone.arrayDays?[currentDayIndex!]
    }
    var transpointsByTime = [SATTransPoint] ()

    let minRadius: CGFloat = 50
    let distanseBetweenPaths: CGFloat = 30
    let buttonRadius: CGFloat = 30

    var x: CGFloat = 0
    var y: CGFloat = 0
    var button = UIButton()

    override func awakeFromNib() {
        super.awakeFromNib()
        createArraysOfTranspointsForCirclePaths()
    }

    func initializeButtons() {
        for transpoint in transpointsByTime {
        var radius: CGFloat
        let index = transpoint.temperatureNumber
        radius = CGFloat(minRadius + CGFloat(index) * distanseBetweenPaths)
        let temperature = SATTemperature.temperatureFrom(transpoint: transpoint)
        let color = temperature.color

        let button = UIButton(type: .system)
        let pan = UIPanGestureRecognizer(target: self,
                                         action: #selector(self.handleTap(_:)))
        button.addGestureRecognizer(pan)

        let position: CGPoint = positionOnCircle(radius: radius,
                                                 center: center,
                                                 time: transpoint.beginTime)
        button.frame = CGRect(x: position.x - buttonRadius/2,
                              y: position.y - buttonRadius/2,
                              width: buttonRadius,
                              height: buttonRadius)
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.clipsToBounds = true
        button.backgroundColor = color
        button.tag = index
        addSubview(button)
        }
    }

    override func draw(_ rect: CGRect) {
    let arcWidth: CGFloat = 5
    var startAngle: CGFloat
    var endAngle: CGFloat
    var color: UIColor

    for transpoint in transpointsByTime {
        var radius: CGFloat
        let temperatureIndex = transpoint.temperatureNumber
        radius = CGFloat(minRadius + CGFloat(temperatureIndex) * distanseBetweenPaths)
        let circlePath = UIBezierPath(arcCenter: center,
                                         radius: radius,
                                     startAngle: CGFloat(0),
                                       endAngle: CGFloat(Double.pi * 2),
                                      clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.gray.cgColor
        shapeLayer.lineWidth = 1.0
        layer.addSublayer(shapeLayer)
        let temperature = SATTemperature.temperatureFrom(transpoint: transpoint)
        color = temperature.color
        startAngle = SATTransPoint.angle(from: transpoint.beginTime)
        let timeIndex = transpointsByTime.index(of: transpoint)
            if timeIndex! < ((transpointsByTime.count) - 1) {
                endAngle = SATTransPoint.angle(from: (transpointsByTime[timeIndex! + 1].beginTime))
            } else {
                endAngle = SATTransPoint.angle(from: 0)
            }
        let path = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        path.lineWidth = arcWidth
        color.setStroke()
        path.stroke()
        }
    }

    func handleTap(_ sender: UITapGestureRecognizer) {
        let queue = DispatchQueue.global(qos: .userInteractive)
  //      queue.async{

            let location = sender.location(in: self)
            let polarLocation: PolarLocation = self.systemLocationToPolar(location: location)

            let button = sender.view as! UIButton!
            self.button = button!
            let radius = CGFloat(self.minRadius + CGFloat(button!.tag) * self.distanseBetweenPaths)

            let newPolarLocation: PolarLocation = PolarLocation.init(radius: radius, angle: polarLocation.angle)
            let newLocation: CGPoint = self.polarLocationToSystem(polarLocation: newPolarLocation)
            self.x = newLocation.x
            self.y = newLocation.y
            let newAngle = atan2(self.center.y - newLocation.y, self.center.x - newLocation.x) + CGFloat(Double.pi)

            let newTime = SATTransPoint.time(from: newAngle)
            for transpoint in self.transpointsByTime {
                if transpoint.temperatureNumber == button?.tag {
                    transpoint.beginTime = newTime
                }
            }

//        }
//            DispatchQueue.main.async {
                self.button.center = CGPoint(x: self.x, y: self.y)
                self.setNeedsDisplay()
//          }
    }

    func getCurrentDayOfWeek() -> Int? {
        let todayDate = NSDate()
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate as Date)
        let index = weekDay - 2
        if index == -1 {
            return 6
        }
        return index
    }

    func positionOnCircle(radius: CGFloat, center: CGPoint, time: Int) -> CGPoint {
        let angle = SATTransPoint.angle(from: time)
        let x: CGFloat = radius * cos(angle)
        let y: CGFloat = radius * sin(angle)
        return CGPoint(x: CGFloat (x+center.x), y: CGFloat (y+center.y))
    }

    func systemLocationToPolar (location: CGPoint) -> PolarLocation {
    let x = location.x - self.center.x
    let y = location.y - self.center.y
    let radius = sqrt(x * x + y * y)

    var angle: CGFloat = 0
        if (x == 0) {
            if (y > 0) {
                angle = CGFloat(Double.pi / 2)
            } else {
                angle -= CGFloat(Double.pi / 2)
            }
        } else {
            angle = self.customAtan(tangens: y / x)
            if (x < 0) {
                if (y >= 0) {
                    angle += CGFloat(Double.pi)
                } else {
                    angle -= CGFloat(Double.pi)
                }
            }
        }
        angle = 0 - angle
    return PolarLocation.init(radius: radius, angle: angle)
}

    func polarLocationToSystem (polarLocation: PolarLocation) -> CGPoint {

    let circleLocationX = polarLocation.radius * cos (0 - polarLocation.angle)
    let circleLocationY = polarLocation.radius * sin (0 - polarLocation.angle)

    let x = circleLocationX + self.center.x
    let y = circleLocationY + self.center.y

        return CGPoint(x: x, y: y)
    }


    func customAtan (tangens: CGFloat) -> CGFloat {

        if tangens > 15.26 {
           return 1.57
        } else if tangens < -15.26 {
            return -1.57
        }

        var i = 0
        while i < 23 && tangens >= self.tangensArray[i] {
            i += 1
        }
        if i == 0 {
            return -1.44
        }

        if i == 23 {
            return 1.44
        }

        if tangens == self.tangensArray[i] {
            return self.anglesArray[i]
        }

        let centralPoint = (self.tangensArray[i] + self.tangensArray[i - 1]) / 2

        if tangens > centralPoint {
            return self.anglesArray[i]
        }

        return self.anglesArray[i - 1]
    }

    func createArraysOfTranspointsForCirclePaths() {
        var transpoints = currentDay?.arrayTranspoints
        if (transpoints?[0].beginTime)! > 0 {
            let previousDayIndex = SATDay.indexOfPreviousWeekday()
            let previousDayTranspoint = currentZone.arrayDays?[previousDayIndex!].arrayTranspoints.last
            let insertionTranspoint = SATTransPoint.init(time: 0, temperatureNumber: (previousDayTranspoint?.temperatureNumber)!)
            transpoints?.append(insertionTranspoint!)
        }
        let transByTime = transpoints?.sorted(by: {($0.beginTime) < ($1.beginTime)})
        transpointsByTime = transByTime!
    }
}

